# askcaesar生成接口文档自动测试框架

## 目录介绍

```
├── .gitignore       // git的忽略列表
├── .mvn             // maven的wrapper目录，通过wrapper能自动下载并配置好maven
│   └── wrapper
│       ├── maven-wrapper.jar
│       └── maven-wrapper.properties   // 配置maven的下载地址
├── README.md         // 本介绍文档
│   ├── ... //其他模块
├── mvnw              // maven wrapper的命令行工具，在类Unix系统使用
├── mvnw.cmd          // maven wrapper的命令行工具，在Windows系统使用
├── pom.xml           // maven父模块配置文件

```

