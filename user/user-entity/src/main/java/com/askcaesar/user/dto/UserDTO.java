package com.askcaesar.user.dto;
import com.askcaesar.basics.util.Convert;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户表
 * @author guopeng
 */
@Data
public class UserDTO extends Convert implements Serializable {

    /**
     * 登录名
     */
    private String loginName;

    /**
     * 登录密码
     */
    private String loginPassword;

    /**
     * 用户全名
     */
    private String userName;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 邮箱地址
     */
    private String email;

    /**
     * 备注说明
     */
    private String remark;
}