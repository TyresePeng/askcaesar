package com.askcaesar.user.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用户表
 * @author guopeng
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginDTO implements Serializable {

    /**signature值*/
    private String  signature;
    /**流水号*/
    private Long  nonce;

}