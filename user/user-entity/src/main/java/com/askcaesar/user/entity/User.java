package com.askcaesar.user.entity;

import com.askcaesar.basics.util.Convert;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * 用户表
 *
 * @author guopeng
 */
@Data
public class User extends Convert implements Serializable {
    private static final long serialVersionUID = 1L;

    /**id*/
    private Integer id;
    /**
     * 登录名
     */
    private String loginName;

    /**
     * 登录密码
     */
    private String loginPassword;

    /**
     * 用户全名
     */
    private String userName;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 邮箱地址
     */
    private String email;

    /**appid*/
    private String appId;

    /**
     * 备注说明
     */
    private String remark;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private LocalDate createdDate;

    /**
     * 修改人
     */
    private String modifiedBy;

    /**
     * 修改时间
     */
    private LocalDate modifiedDate;

    /**
     * 用户头像上传记录Id
     */
    private String userAvatarPath;

    /**
     * 上次登录时间
     */
    private LocalDate lastLoginTime;


}