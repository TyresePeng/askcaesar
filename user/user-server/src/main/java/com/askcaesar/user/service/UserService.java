package com.askcaesar.user.service;

import com.askcaesar.user.dto.LoginDTO;

/**
 * 用户服务
 * @author GuoPeng
 * @createTime 2021-05-27 14:33:00
 */
public interface UserService {

    /**
     * @param loginName 登录名称,loginPassword 登录密码
     * @return UserDTO
     * @description 登录
     * @date 2021-05-27
     */
    String lonin(String loginName, String loginPassword);


    /**
     * @param param 入参
     * @param reqUri 接口uri
     * @param appId appid
     * @return LoginDTO 认证信息
     */
    LoginDTO getSignature(String param,String reqUri, String appId);
}
