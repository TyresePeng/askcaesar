package com.askcaesar.user.controller;

import com.askcaesar.basics.common.ApiResponse;
import com.askcaesar.basics.dto.SignatureDTO;
import com.askcaesar.basics.util.RequestWrapper;
import com.askcaesar.user.dto.LoginDTO;
import com.askcaesar.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * @author GuoPeng
 * @version 1.0.0
 * @description 用户服务
 * @createTime 2021-05-27 14:33:00
 */
@RestController
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * @param loginName     登录用户名
     * @param loginPassword 登录用密码
     * @return ApiResponse<com.askcaesar.user.dto.UserDTO>
     * @description
     * @date 2021-06-09
     */
    @GetMapping("/login")
    public ApiResponse<String> login(@RequestParam("loginName") String loginName,
                                     @RequestParam("loginPassword") String loginPassword) {
        return ApiResponse.success(userService.lonin(loginName, loginPassword));
    }

    /**
     * @param appId appId
     * @return ApiResponse<com.askcaesar.user.dto.UserDTO>
     * @description
     * @date 2021-06-09
     */
    @PostMapping("/get-signature")
    public ApiResponse<LoginDTO> getSignature(HttpServletRequest req, String appId,String reqUri) {
        RequestWrapper requestWrapper = new RequestWrapper(req);
        log.info("入参=》#{}",requestWrapper.getBody());
        return ApiResponse.success(userService.getSignature(requestWrapper.getBody(),reqUri, appId));
    }


    /*==============测试================*/

    @GetMapping("/test")
    public ApiResponse<Boolean> test(SignatureDTO param) {
        return ApiResponse.success(true);
    }

    /**TODO  不支持fromData传参待优化  Rest json入参都可*/
    @PostMapping("/test")
    public ApiResponse<Boolean> post(SignatureDTO param) {
        return ApiResponse.success(true);
    }

    @PostMapping("/testjson")
    public ApiResponse<Boolean> postjson(@RequestBody SignatureDTO param) {
        return ApiResponse.success(true);
    }

    @PutMapping("/testPath/{id}")
    public ApiResponse<String> postjson(@PathVariable("id")String id ) {
        return ApiResponse.success(id);
    }

}
