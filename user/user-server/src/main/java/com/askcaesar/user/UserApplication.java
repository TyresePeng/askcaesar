package com.askcaesar.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * @author GuoPeng
 */
@SpringBootApplication(scanBasePackages = {"com.askcaesar.user",
        "com.askcaesar.commons"})
/**mybatis包扫描配置*/
@MapperScan({"com.askcaesar.user.mapper"})
@EnableFeignClients(basePackages = {"com.askcaesar.interfaces"})
@EnableDiscoveryClient
public class UserApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }

}
