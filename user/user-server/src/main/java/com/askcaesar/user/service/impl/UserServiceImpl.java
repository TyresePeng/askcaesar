package com.askcaesar.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.askcaesar.basics.dto.SignatureDTO;
import com.askcaesar.basics.exception.BusinessException;
import com.askcaesar.basics.util.RedisUtil;
import com.askcaesar.basics.util.SignatureUtil;
import com.askcaesar.basics.util.SnowflakeIdUtil;
import com.askcaesar.commons.databases.intefaces.MyServiceImpl;
import com.askcaesar.user.dto.LoginDTO;
import com.askcaesar.user.entity.User;
import com.askcaesar.user.mapper.UserMapper;
import com.askcaesar.user.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * @author 用户服务实现
 * @createTime 2021-05-27 14:33:00
 */
@Service
@Log4j2
public class UserServiceImpl extends MyServiceImpl<UserMapper> implements UserService {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SignatureUtil signatureUtil;

    @Override
    public String lonin(String loginName, String loginPassword) {
        //拿出appid 放入redis进行注册登录
        User user = baseMapper.selectLoginInfo(loginName, loginPassword);
        if (Objects.nonNull(user)) {
            // TODO 可以修改为其他唯一值生成
            user.setAppId(UUID.randomUUID().toString().replace("-", ""));
            //2个小时登录有效
            redisUtil.set(SignatureUtil.APPID + user.getAppId(), JSON.toJSONString(user), 60 * 60 * 60 * 2);
            user.setLastLoginTime(LocalDate.now());
            baseMapper.updateByPrimaryKeySelective(user);
            return user.getAppId();
        }
        throw new BusinessException("请检查账户密码在进行登录");
    }

    @Override
    public LoginDTO getSignature(String param, String reqUri, String appId) {
        if ( redisUtil.hasKey(SignatureUtil.APPID + appId)) {
            Long nonce = SnowflakeIdUtil.newId();
            SignatureDTO signatureParam = SignatureDTO.builder()
                    .appId(appId)
                    .reqUri(reqUri)
                    .nonce(nonce).expirationTime(SignatureDTO.EXPIRATIONTIMEOUT).build();

            log.info("请求流水号:#{}",nonce);
            //5分钟有效
            String signature = signatureUtil.encrypt(signatureParam, param, true);
            return LoginDTO.builder().signature(signature).nonce(nonce).build();
        }
        throw new BusinessException("登录状态已过期");
    }

}
