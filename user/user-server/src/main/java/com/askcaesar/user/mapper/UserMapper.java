package com.askcaesar.user.mapper;


import com.askcaesar.commons.databases.intefaces.MyBatisBaseDao;
import com.askcaesar.user.entity.User;

public interface UserMapper extends MyBatisBaseDao<User,Integer>  {

    User selectLoginInfo(String loginName, String loginPassword);
}