package com.askcaesar.interfaces.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

/**
 * workspaces_info
 * @author 
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkspacesInfo {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 工作空间名称
     */
    private String workspacesName;

    /**
     * 空间描述
     */
    private String summary;

    /**
     * 工作空间类型（团队1/个人2）
     */
    private String workspacesType;

    /**
     * 工作空间开放类型 （0私有 1开源 ）
     */
    private Boolean publicType;

    /**
     * 创建人id
     */
    private Integer createUserId;

    /**
     * 创建时间
     */
    @JsonFormat(shape =JsonFormat.Shape.STRING,pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 逻辑删除
     */
    private Boolean deleteBit;

}