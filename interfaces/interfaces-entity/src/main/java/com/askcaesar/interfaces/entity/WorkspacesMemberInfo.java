package com.askcaesar.interfaces.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * workspaces_member_info
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkspacesMemberInfo implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 工作空间id
     */
    private Integer workspacesId;

    /**
     * 所属工作空间权限 (admin dev look)
     */
    private Integer userRole;

    /**
     * 添加人id
     */
    private Integer addUserId;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

    /**
     * 逻辑删除
     */
    private Boolean deleteBit;

    private static final long serialVersionUID = 1L;
}