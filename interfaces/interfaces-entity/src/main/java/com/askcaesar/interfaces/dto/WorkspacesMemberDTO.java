package com.askcaesar.interfaces.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 工作空间成员基本数据对象
 * @author ZhongYuXing
 * @since 2021-06-15 17:39:33
 * */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkspacesMemberDTO {
    private Integer userId;

    private String userRole;

    private Integer addUserId;
}
