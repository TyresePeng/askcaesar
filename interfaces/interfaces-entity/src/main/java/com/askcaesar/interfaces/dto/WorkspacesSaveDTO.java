package com.askcaesar.interfaces.dto;

import lombok.*;

import java.util.List;

/**
 * @description 保存工作空间提交实体类
 * @author ZhongYuXing
 * @since 2021-06-15 17:39:33
 * */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkspacesSaveDTO {

    /** 工作空间名称*/
    private String workspacesName;

    /** 空间描述*/
    private String summary;

    /** 工作空间类型 团队1/个人2*/
    private Short workspacesType;

    /** 0私有 1开源*/
    private Boolean publicType;

    /** 成员列表*/
    private List<WorkspacesMemberDTO> members;
}
