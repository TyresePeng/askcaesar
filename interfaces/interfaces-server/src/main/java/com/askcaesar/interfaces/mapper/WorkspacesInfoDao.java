package com.askcaesar.interfaces.mapper;

import com.askcaesar.commons.databases.intefaces.MyBatisBaseDao;
import com.askcaesar.interfaces.entity.WorkspacesInfo;

/**
 * WorkspacesInfoDao继承基类
 */
public interface WorkspacesInfoDao extends MyBatisBaseDao<WorkspacesInfo, Integer> {
}