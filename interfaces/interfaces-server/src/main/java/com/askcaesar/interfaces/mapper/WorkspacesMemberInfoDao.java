package com.askcaesar.interfaces.mapper;

import com.askcaesar.commons.databases.intefaces.MyBatisBaseDao;
import com.askcaesar.interfaces.entity.WorkspacesMemberInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WorkspacesMemberInfoDao  extends MyBatisBaseDao<WorkspacesMemberInfo, Integer> {

    Integer batchInsert(@Param("datas") List<WorkspacesMemberInfo> datas);

}