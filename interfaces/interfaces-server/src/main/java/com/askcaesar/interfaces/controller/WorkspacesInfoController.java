package com.askcaesar.interfaces.controller;

import com.askcaesar.basics.common.ApiResponse;
import com.askcaesar.basics.common.BaseController;
import com.askcaesar.interfaces.dto.WorkspacesSaveDTO;
import com.askcaesar.interfaces.service.WorkspacesInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/workspaces")
public class WorkspacesInfoController extends BaseController {


    @Autowired
    private WorkspacesInfoService workspacesInfoService;



    /**
     *
     * @description 用户创建一个工作空间
     * @author ZhongYuXing
     * @since 2021-06-15 17:39:33
     * @param param 保存对象
     * */
    @PostMapping
    public ApiResponse<Boolean> createWorkspaces(@RequestBody WorkspacesSaveDTO param){
        return success(workspacesInfoService.newWorkspaceForTheFirstTime(param));
    }


}
