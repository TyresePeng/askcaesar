package com.askcaesar.interfaces.config;

import com.askcaesar.basics.interceptor.ContextHandlerInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * 拦截器配置
 * @author ZhongYuXing
 * @date 2021-06-16 13:29:33
 * */
@Configuration
public class InterfaceprotConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ContextHandlerInterceptor()).addPathPatterns("/**");
    }

}
