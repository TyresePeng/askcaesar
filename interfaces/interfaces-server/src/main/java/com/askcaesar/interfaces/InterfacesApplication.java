package com.askcaesar.interfaces;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication(scanBasePackages = {"com.askcaesar.interfaces","com.askcaesar.commons.databases"})
@MapperScan({"com.askcaesar.interfaces.mapper"})
@EnableDiscoveryClient
public class InterfacesApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterfacesApplication.class, args);
    }
}
