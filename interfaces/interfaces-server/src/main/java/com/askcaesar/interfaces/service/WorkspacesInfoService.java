package com.askcaesar.interfaces.service;

import com.askcaesar.interfaces.dto.WorkspacesSaveDTO;

public interface WorkspacesInfoService {


    Boolean newWorkspaceForTheFirstTime(WorkspacesSaveDTO param);

}
