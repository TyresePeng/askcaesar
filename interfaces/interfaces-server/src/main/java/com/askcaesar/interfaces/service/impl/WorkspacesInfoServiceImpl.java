package com.askcaesar.interfaces.service.impl;

import com.askcaesar.basics.common.PresentContextInfo;
import com.askcaesar.interfaces.dto.WorkspacesMemberDTO;
import com.askcaesar.interfaces.dto.WorkspacesSaveDTO;
import com.askcaesar.interfaces.entity.WorkspacesInfo;
import com.askcaesar.interfaces.entity.WorkspacesMemberInfo;
import com.askcaesar.interfaces.mapper.WorkspacesInfoDao;
import com.askcaesar.interfaces.mapper.WorkspacesMemberInfoDao;
import com.askcaesar.interfaces.service.WorkspacesInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @description 工作空间数据服务层
 * @author ZhongYuXing
 * @since 2021-06-15 17:39:33
 * */
@Service
public class WorkspacesInfoServiceImpl implements WorkspacesInfoService {

    /**
     * 工作空间数据层
     * */
    @Autowired
    private WorkspacesInfoDao workspacesInfoDao;

    /**
     * 工作空间成员数据
     * */
    @Autowired
    private WorkspacesMemberInfoDao workspacesMemberInfoDao;


    @Override
    @Transactional(propagation= Propagation.REQUIRES_NEW,rollbackFor = {Exception.class})
    public Boolean newWorkspaceForTheFirstTime(WorkspacesSaveDTO param) {
        LocalDateTime nowTime = LocalDateTime.now();
        WorkspacesInfo workspacesInfo = WorkspacesInfo.builder()
                .workspacesName(param.getWorkspacesName())
                .summary(param.getSummary())
                .workspacesType(String.valueOf(param.getWorkspacesType()))
                .publicType(param.getPublicType())
                .createUserId( Integer.valueOf(Optional.<String>ofNullable(PresentContextInfo.getUserId())
                        .orElseGet(() -> "0")))
                .createTime(nowTime)
                .deleteBit(Boolean.FALSE)
                .build();
        int insert = workspacesInfoDao.insert(workspacesInfo);
        Assert.isTrue(insert == 1,"保存工作空间失败！");
        // 添加成员数据 如果为空或者[] 则添加自己
        List<WorkspacesMemberDTO> members =
                param.getMembers();
        // 插入数据结果
        List<WorkspacesMemberInfo> insertsMembers = new ArrayList<>();
        Boolean haveOneself = Boolean.TRUE;
        if (!CollectionUtils.isEmpty(members)) {
            for (WorkspacesMemberDTO member : members) {
                if (member.getUserId().equals(workspacesInfo.getCreateUserId())) {
                    // 等于添加人userId
                    haveOneself = Boolean.FALSE;
                }
                insertsMembers.add(WorkspacesMemberInfo.builder()
                        .addTime(nowTime)
                        .workspacesId(workspacesInfo.getId())
                        .addUserId(workspacesInfo.getCreateUserId())
                        .userRole(Integer.valueOf(member.getUserRole()))
                        .userId(member.getUserId())
                        .deleteBit(Boolean.FALSE)
                        .build());
            }
        }

        if (haveOneself) {
            // 没有自己添加一个默认的自己
            insertsMembers.add(WorkspacesMemberInfo.builder()
                    .addTime(nowTime)
                    .workspacesId(workspacesInfo.getId())
                    .addUserId(workspacesInfo.getCreateUserId())
                    .userRole(0)
                    .userId(workspacesInfo.getCreateUserId())
                    .deleteBit(Boolean.FALSE)
                    .build());
        }
        Integer integer = workspacesMemberInfoDao.batchInsert(insertsMembers);
        Assert.isTrue(integer == insertsMembers.size(),"保存成员失败！");
        return Boolean.TRUE;
    }
}
