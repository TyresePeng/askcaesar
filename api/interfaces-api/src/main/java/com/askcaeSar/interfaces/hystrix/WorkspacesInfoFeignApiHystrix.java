package com.askcaeSar.interfaces.hystrix;


import com.askcaeSar.interfaces.api.WorkspacesInfoFeignApi;
import com.askcaesar.basics.common.ApiResponse;
import com.askcaesar.interfaces.entity.WorkspacesInfo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WorkspacesInfoFeignApiHystrix implements WorkspacesInfoFeignApi {
    @Override
    public ApiResponse<List<WorkspacesInfo>> userWorkspaces() {
        return ApiResponse.fail();
    }
}
