package com.askcaeSar.interfaces.api;


import com.askcaeSar.interfaces.hystrix.WorkspacesInfoFeignApiHystrix;
import com.askcaesar.basics.common.ApiResponse;
import com.askcaesar.interfaces.entity.WorkspacesInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "interfaces-server",path = "/workspaces",fallback = WorkspacesInfoFeignApiHystrix.class)
public interface WorkspacesInfoFeignApi {

    @GetMapping("/user")
    ApiResponse<List<WorkspacesInfo>> userWorkspaces();

}
