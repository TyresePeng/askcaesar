package com.askcaesar.zuul.bean;

import com.askcaesar.basics.util.RedisUtil;
import com.askcaesar.basics.util.SignatureUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author GuoPeng
 * @version 1.0.0
 * @description bean注册
 * @createTime 2021-06-09 20:21:00
 */
@Component
public class Register {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * redis工具类注册
     *
     * @return RedisUtil
     * @date 2021-06-09
     */
    @Bean
    public RedisUtil redisUtil() {
        return new RedisUtil(redisTemplate);
    }

    /**
     * signature工具类
     * @return signatureUtil
     * @date 2021-06-09
     */
    @Bean
    public SignatureUtil signatureUtil() {
        return new SignatureUtil(new RedisUtil(redisTemplate));
    }
}
