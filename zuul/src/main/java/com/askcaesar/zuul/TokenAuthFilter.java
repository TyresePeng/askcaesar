package com.askcaesar.zuul;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.askcaesar.basics.common.ApiResponse;
import com.askcaesar.basics.dto.SignatureDTO;
import com.askcaesar.basics.enums.PresentContextEnum;
import com.askcaesar.basics.enums.ReturnCode;
import com.askcaesar.basics.exception.BusinessException;
import com.askcaesar.basics.exception.SignatureException;
import com.askcaesar.basics.util.RedisUtil;
import com.askcaesar.basics.util.RequestWrapper;
import com.askcaesar.basics.util.SignatureUtil;
import com.askcaesar.user.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.context.RequestContext;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.*;

@Component
@Log4j2
public class TokenAuthFilter extends ZuulFilter {

    private static final AntPathMatcher ANT_PATH_MATCHER = new AntPathMatcher();

    /**
     * <p> @Title: steerUrl </p>
     * <p> @Description: 放行 接口 </p>
     * <p> @param   </p>
     * <p> @return  </p>
     * <p> @Author GuoPeng </p>
     */
    public static final List<String> ignoreList = Arrays.asList(
            "/**/api/user/login",
            "/**/api/user/get-signature/**"
    );

    /**
     * signature
     */
    private static final String SIGNATURE = "signature";

    /**
     * 请求流水号
     */
    private static final String NONCE = "nonce";

    /**
     * 请求时间戳
     */
    private static final String TIMESTAMP = "timestamp";

    /**
     * appid
     */
    private static final String APPID = "appId";

    /**
     * UTF8
     */
    private static final String UTF8 = "UTF-8";

    /**
     * UTF8
     */
    private static final String RESPTYPE = "application/json; charset=utf-8";

    @Autowired
    private SignatureUtil signatureUtil;

    @Autowired
    private RedisUtil redisUtil;


    /***
     * 是否执行该过滤器，此处为true，说明需要过滤
     * @return boolean
     */
    @Override
    public boolean shouldFilter() {
        String thisRequestURI = RequestContext.getCurrentContext().getRequest().getRequestURI();
        return !ignoreList.stream().anyMatch((url) ->
                thisRequestURI.startsWith(url) || ANT_PATH_MATCHER.match(url, thisRequestURI));
    }


    /***
     * 过滤后执行
     * @return int
     */
    @SneakyThrows
    @Override
    public Object run() {

        // 获取请求上下文
        RequestContext context = RequestContext.getCurrentContext();
        // 获取到request
        HttpServletRequest request = context.getRequest();
        context.getResponse().setContentType(RESPTYPE);
        context.getResponse().setCharacterEncoding(UTF8);
        context.setSendZuulResponse(true);
        context.setResponseStatusCode(HttpStatus.OK.value());


        //头部信息
        String signature = request.getHeader(SIGNATURE);
        String appid = request.getHeader(APPID);
        String timestamp = request.getHeader(TIMESTAMP);
        String nonce = request.getHeader(NONCE);

        //设置头部信息
        ObjectMapper objectMapper = null;
        SignatureDTO build = null;
        if(StringUtils.isEmpty(signature)||StringUtils.isEmpty(appid)||
                StringUtils.isEmpty(timestamp)||StringUtils.isEmpty(nonce)){
            ApiResponse apiResponse = new ApiResponse(ReturnCode.SIGN_ERROR.getCode(), ReturnCode.SIGN_ERROR.getMsg(), null, null);
            context.setResponseStatusCode(ReturnCode.SIGN_ERROR.getCode());
            context.setResponseBody(objectMapper.writeValueAsString(apiResponse));
            context.setSendZuulResponse(false);
            return null;
        }
        try {
            objectMapper = new ObjectMapper();
            build = SignatureDTO.builder().signature(signature)
                    .expirationTime(SignatureDTO.EXPIRATIONTIMEOUT)
                    .appId(appid)
                    .timestamp(Long.valueOf(timestamp))
                    .reqUri(request.getRequestURI())
                    .nonce(Long.valueOf(nonce)).build();
        } catch (NumberFormatException e) {
            ApiResponse apiResponse = new ApiResponse(ReturnCode.SIGN_ERROR.getCode(), ReturnCode.SIGN_ERROR.getMsg(), e.getMessage(), null);
            context.setResponseStatusCode(ReturnCode.SIGN_ERROR.getCode());
            context.setResponseBody(objectMapper.writeValueAsString(apiResponse));
            context.setSendZuulResponse(false);
            return null;
        }

        Boolean verify = false;
        RequestWrapper requestWrapper = new RequestWrapper(request);
        context.setRequest(requestWrapper);
        try {
            verify = signatureUtil.verify(build, requestWrapper.getBody());
        } catch (SignatureException e) {
            ApiResponse apiResponse = new ApiResponse(ReturnCode.SIGN_ERROR.getCode(), ReturnCode.SIGN_ERROR.getMsg(), e.getMessage(), null);
            context.setResponseStatusCode(ReturnCode.SIGN_ERROR.getCode());
            context.setResponseBody(objectMapper.writeValueAsString(apiResponse));
            context.setSendZuulResponse(false);
            return null;
        }
        //校验未通过
        if (!verify) {
            ApiResponse apiResponse = new ApiResponse(ReturnCode.SIGN_ERROR.getCode(), null, ReturnCode.SIGN_ERROR.getMsg(), null);
            context.setResponseStatusCode(ReturnCode.SIGN_ERROR.getCode());
            context.setResponseBody(objectMapper.writeValueAsString(apiResponse));
            context.setSendZuulResponse(false);
        }
        this.setContextContent(appid);
        return null;
    }



    /***
     * 过滤器优先级
     * @return int
     */
    @Override
    public int filterOrder() {
        return FilterConstants.SERVLET_DETECTION_FILTER_ORDER - 1;
    }

    /***
     * 路由前过滤
     * @return String
     */
    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    /** 设置上下文内容*/
    public void setContextContent(String appId){
        //获得Request请求
        RequestContext ctx = RequestContext.getCurrentContext();
        // TODO 应该使用序列化而不是JSON
        Object userJson = redisUtil.get(SignatureUtil.APPID + appId);
        if (Objects.isNull(userJson)) {
            throw new BusinessException("登录状态已过期");
        }
        User user = JSON.parseObject((String)userJson, User.class);
        addHeader(ctx, PresentContextEnum.USER_ID.name(), user.getId());
        addHeader(ctx, PresentContextEnum.USER_NAME.name(), user.getUserName());
        addHeader(ctx, PresentContextEnum.EMAIL.name(), user.getEmail());
        addHeader(ctx, PresentContextEnum.APP_ID.name(), appId);
    }

    /** 设置请求头*/
    private void addHeader(RequestContext ctx, String name, Object value) {
        if (StringUtils.isEmpty(value)) {
            return;
        }
        String valueEncode = "";
        try {
            valueEncode = URLEncoder.encode(value.toString(), "utf-8");
        } catch (Exception e) {
        }
        ctx.addZuulRequestHeader(name, valueEncode);
    }

}