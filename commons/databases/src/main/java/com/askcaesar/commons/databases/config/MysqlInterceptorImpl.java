package com.askcaesar.commons.databases.config;

import com.askcaesar.commons.databases.intefaces.MysqlInterceptor;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Component;

/**
 * **********************************************
 * @Intercepts定义Signature数组,因此可以拦截多个,但是只能拦截类型为：
 *         Executor
 *         ParameterHandler
 *         StatementHandler
 *         ResultSetHandler
 * @Description: 数据库操作性能拦截器实现,记录耗时
 * @Author: GuoPeng
 * @Company: CommonConstant
 * @Version: 1.0
 ************************************************/
@Intercepts(value = {
        @Signature (type=Executor.class,
                method="update",
                args={MappedStatement.class,Object.class}),
        @Signature(type=Executor.class,
                method="query",
                args={MappedStatement.class,Object.class, RowBounds.class, ResultHandler.class,
                        CacheKey.class, BoundSql.class}),
        @Signature(type=Executor.class,
                method="query",
                args={MappedStatement.class,Object.class,RowBounds.class,ResultHandler.class})})
@Component
public class MysqlInterceptorImpl extends MysqlInterceptor implements Interceptor {

    @Override
    public Object plugin(Object target) {
        if (target instanceof Executor) {
            return Plugin.wrap(target, this);
        }
        return target;
    }
}
