package com.askcaesar.basics.util;

import com.askcaesar.basics.common.ApiAssert;
import com.askcaesar.basics.exception.BusinessException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpMethod;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StreamUtils;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

@Log4j2
public class RequestWrapper extends HttpServletRequestWrapper {

    private String body;

    private static final String RESPTYPEJSON = "application/json";

    private static final String RESPFORMDATA = "multipart/form-data;";


    private static final String APPID = "appId";

    private static final String REQURI = "reqUri";


    public RequestWrapper(HttpServletRequest request) {
        /**TODO postMan fromData 传参拿不到参数 可优化*/
        super(request);
        if (RESPTYPEJSON.equalsIgnoreCase(request.getContentType())) {
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader bufferedReader = null;
            InputStream inputStream = null;
            try {
                inputStream = request.getInputStream();
                if (inputStream != null) {
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    char[] charBuffer = new char[128];
                    int bytesRead = -1;
                    while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                        stringBuilder.append(charBuffer, 0, bytesRead);
                    }
                } else {
                    stringBuilder.append("");
                }
            } catch (IOException ex) {
                throw new BusinessException("签名入参转换异常");
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.body = stringBuilder.toString();
        } else {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                Map<String, String[]> parameterMap = new HashMap<>(request.getParameterMap());
                if (!ObjectUtils.isEmpty(parameterMap.get(APPID))) {
                    parameterMap.remove(APPID);
                }
                if (!ObjectUtils.isEmpty(parameterMap.get(REQURI))) {
                    parameterMap.remove(REQURI);
                }
                this.body = objectMapper.writeValueAsString(parameterMap);
            } catch (JsonProcessingException e) {
                log.error("请求转换器，序列化入参异常#{}", e.getMessage());
                ApiAssert.istrue("请求转换器，序列化入参异常");
            }
        }

        /**TODO  fromData 校验通过  zuul网关报错未解决*/
        if (request.getContentType().startsWith(RESPFORMDATA)) {
            getRequestParams(request);
        }

    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(body.getBytes());
        ServletInputStream servletInputStream = new ServletInputStream() {
            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {
            }

            @Override
            public int read() throws IOException {
                return byteArrayInputStream.read();
            }
        };
        return servletInputStream;

    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(this.getInputStream()));
    }

    public String getBody() {
        return this.body;
    }

    /**
     * 获取请求参数
     *
     * @param request
     * @return
     * @throws IOException
     */
    public void getRequestParams(HttpServletRequest request) {
        String httpMethod = request.getMethod();
        if (HttpMethod.POST.matches(httpMethod)) {
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader bufferedReader = null;
            InputStream stream =null;
            try {
                 stream = request.getInputStream();
                if (stream != null) {
                    bufferedReader = new BufferedReader(new InputStreamReader(stream));
                    char[] charBuffer = new char[128];
                    int bytesRead = -1;
                    while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                        stringBuilder.append(charBuffer, 0, bytesRead);
                    }
                }
                String body = stringBuilder.toString();
                String[] split = body.split("\r\n");
                int keyIndex = 2;
                int valesIndex = 4;
                //计算入参
                Map<String, String[]> param = new HashMap<>((split.length - 1) / 4);
                for (int i = 0; i < (split.length - 1) / 4; i++) {
                    String key = split[keyIndex - 1];
                    key = key.replaceAll("Content-Disposition: form-data; name=", "");
                    key = key.substring(1, key.length() - 1);
                    String vales = split[valesIndex - 1];
                    param.put(key, new String[]{vales});
                    keyIndex += 4;
                    valesIndex += 4;

                }
                ObjectMapper objectMapper = new ObjectMapper();
                this.body = objectMapper.writeValueAsString(param);
            } catch (IOException e) {
                throw new BusinessException("签名入参转换异常");
            }finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }
}