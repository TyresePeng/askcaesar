package com.askcaesar.basics.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p> @author GuoPeng</p>
 * <p> @version 1.0.0</p>
 * <p> @description SignatureDTO</p>
 * <p> @createTime 2021-05-17 11:06:00</p>
 */
@Data
@AllArgsConstructor
@Builder
public class SignatureDTO {

    public static final Long EXPIRATIONTIMEOUT=1000*60*5L;

    /**signature值*/
    private String  signature;
    /**流水号*/
    private Long  nonce;
    /**请求时间*/
    private  Long timestamp;
    /**过期时间*/
    private   Long expirationTime;
    /**appid*/
    private String appId;

    /**请求地址*/
    private String reqUri;
}
