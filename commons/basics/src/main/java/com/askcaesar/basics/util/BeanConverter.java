package com.askcaesar.basics.util;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Constructor;
import java.util.*;
import java.util.stream.Collectors;


/**
 * 转换工具类
 * @author GuoPeng
 */
@Log4j2
public class BeanConverter {
    /**
     * 单个对象转换 (用于 实体转换dto)
     * @param targetClass 目标对象
     * @param source      源对象
     * @return 转换后的目标对象
     */
    public static <T> T convert(Class<T> targetClass, Object source)  {
        T target = null;
        try {
            target = targetClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            log.error("类型转换异常#{}",e.getMessage());
            throw new RuntimeException("类转换失败", e);
        }
        BeanUtils.copyProperties(source,target);
        return target;
    }

    /**
     * 列表转换
     *
     * @param clazz the clazz
     * @param list  the list
     */
    public static <T> List<T> convert(Class<T> clazz, List<?> list) {
        return CollectionUtils.isEmpty(list) ? Collections.emptyList() : list.stream().map(e -> convert(clazz, e)).collect(Collectors.toList());
    }
}
