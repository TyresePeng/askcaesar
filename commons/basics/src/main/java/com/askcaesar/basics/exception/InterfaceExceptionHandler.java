package com.askcaesar.basics.exception;

import com.askcaesar.basics.common.ApiResponse;
import com.askcaesar.basics.enums.ReturnCode;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 自定义全局拦截器
 *
 * @author GuoPeng
 */
@Slf4j
@RestControllerAdvice
public class InterfaceExceptionHandler {

    /**
     * 接口 业务异常
     */
    @ResponseBody
    @ExceptionHandler(BusinessException.class)
    public ApiResponse businessInterfaceException(BusinessException e) {
        log.error(e.getMessage());
        return new ApiResponse(ReturnCode.BUSINESS_ERROR.getCode(), ReturnCode.BUSINESS_ERROR.getMsg(), e.getMessage(), null);
    }

}