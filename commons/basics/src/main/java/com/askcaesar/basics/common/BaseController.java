package com.askcaesar.basics.common;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BaseController {

    @Resource
    protected HttpServletRequest request;
    @Resource
    protected HttpServletResponse response;


    /**
     * 带业务数据的成功响应
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ApiResponse<T> success(T data) {
        return ApiResponse.success(data);
    }

    /**
     * 响应失败
     * @return
     */
    public static ApiResponse fail(String msg) {
        return ApiResponse.fail(msg);
    }

}
