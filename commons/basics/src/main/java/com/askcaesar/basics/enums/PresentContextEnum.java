package com.askcaesar.basics.enums;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
/**
 * @description 上下文参数key
 * @author ZhongYuXing
 * @since 2021-06-15 17:39:33
 * */
@AllArgsConstructor
@NoArgsConstructor
public enum PresentContextEnum {
        USER_ID("USER_ID","用户id"),
        USER_NAME("USER_NAME","用户名称"),
        EMAIL("EMAIL","用户邮箱"),
        APP_ID("APP_ID","APPID"),
    ;


    private String name;
    private String description;


}
