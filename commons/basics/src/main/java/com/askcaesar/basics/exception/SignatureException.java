package com.askcaesar.basics.exception;

/**
 * @author GuoPeng
 * @version 1.0.0
 * @description 登录异常
 * @createTime 2021-06-10 10:24:00
 */
public class SignatureException extends RuntimeException{

    public SignatureException(String message) {
        super(message);
    }

    public SignatureException(String message, Throwable cause) {
        super(message, cause);
    }

    public SignatureException(Throwable cause) {
        super(cause);
    }

    public SignatureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public SignatureException() {
    }
}
