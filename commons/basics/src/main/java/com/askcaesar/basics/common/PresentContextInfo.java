package com.askcaesar.basics.common;

import com.askcaesar.basics.enums.PresentContextEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @description 当前请求上下文
 * @author ZhongYuXing
 * @since 2021-06-16 17:39:33
 * */
public class PresentContextInfo {
    /** 当前线程上下文数据*/
    public static final ThreadLocal<Map<String, String>> THREAD_LOCAL = new ThreadLocal<>();

    public static Map<String, String> getLocalMap() {
        Map<String, String> map = THREAD_LOCAL.get();
        if (map == null) {
            map = new HashMap<>(10);
            THREAD_LOCAL.set(map);
        }
        return map;
    }

    public static void set(String key, String value) {
        Map<String, String> map = getLocalMap();
        map.put(key, value == null ? "" : value);
    }

    public static final String get(String key) {
        Map<String, String> map = getLocalMap();
        return map.getOrDefault(key, "");
    }


    public static final String getUserId(){
        return get(PresentContextEnum.USER_ID.name());
    }
    public static final String getUserName(){
        return get(PresentContextEnum.USER_NAME.name());
    }
    public static final String getEmail(){
        return get(PresentContextEnum.EMAIL.name());
    }
    public static final String getAppId(){
        return get(PresentContextEnum.APP_ID.name());
    }





}
