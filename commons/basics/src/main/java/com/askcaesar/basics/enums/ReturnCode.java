package com.askcaesar.basics.enums;

/**
 * 公共业务返回常量
 *
 * @author GuoPeng
 */
public enum ReturnCode {

    /**
     * 请求成功
     */
    SUCCESS(0, "SUCCESS"),
    /**
     * 请求失败
     */
    FAIL(-1, "ERROR"),

    /**
     * 请进行登录验证
     */
    NOT_LOG_IN(403, "请求被拒绝访问，请先进行登录认证"),

    /**
     * 参数不合法
     */
    PARAMETER_INVALID(3, "参数不合法"),
    /**
     * 系统异常
     */
    SYSTEM_ERROR(4, "系统异常"),

    /**
     * 验证失败
     */
    SIGN_ERROR(401, "验签异常"),
    /**
     * 业务异常
     */
    BUSINESS_ERROR(6, "业务异常");

    private final Integer code;
    private final String msg;


    ReturnCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
