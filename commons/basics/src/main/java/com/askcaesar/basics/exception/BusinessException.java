package com.askcaesar.basics.exception;

/**
 * @author GuoPeng
 * @version 1.0.0
 * @description 业务异常,全局拦截接口
 * @createTime 2021-06-10 11:05:00
 */
public class BusinessException extends RuntimeException{

    public BusinessException() {

    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
