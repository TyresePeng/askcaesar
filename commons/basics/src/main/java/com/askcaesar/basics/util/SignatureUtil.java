package com.askcaesar.basics.util;

import com.askcaesar.basics.dto.SignatureDTO;
import com.askcaesar.basics.exception.SignatureException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author GuoPeng
 * @version 1.0.0
 * @description TODO
 * @createTime 2021-05-15 11:26:00
 */
@Slf4j
public class SignatureUtil {


    private RedisUtil redisUtil;

    public SignatureUtil(RedisUtil redisUtil){
        this.redisUtil=redisUtil;
    }

    public final static String NONCE="_nonce";

    public final static String APPID="appid_";

    /**
     *
     * 防止伪装攻击
     * 防伪令牌可以避免伪装攻击。
     *
     * 防止篡改攻击
     * 签名是防止参数被篡改的一种验证方式
     * 如果参数项/参数值被修改，修改的请求参数最后验签肯定是false
     *
     * 防重放攻击
     * 随机字符串：唯一数据或者可以视为一段时间内的唯一数据
     *
     *
     * <p> @description signature 加密</p>
     * <p> @author GuoPeng </p>
     * <p> @param signatureDTO ,nonce 请求流水号,param 请求入参,isCache 是否缓存</p>
     * <p> @time 2021-05-15 14:29</p>
     * <p> @return String</p>
     */
    public String encrypt(SignatureDTO signatureDTO, String paramJson, Boolean isCache) {
        Object appsecret = redisUtil.get(APPID + signatureDTO.getAppId());
        if (ObjectUtils.isEmpty(appsecret)) {
            throw new SignatureException("请登录,检查Appsecret重试!");
        }

        EncryptUtil instance = EncryptUtil.getInstance();
        //入参
        String paramCode = sortJson(paramJson);
        String signature = instance.SHA1(paramCode, appsecret.toString()+signatureDTO.getReqUri());
        if (isCache) {
            redisUtil.set(signatureDTO.getNonce() + NONCE, signatureDTO.getNonce() + NONCE, signatureDTO.getExpirationTime());
        }
        return signature;
    }

    /**
     * <p> @description 校验 signature</p>
     * <p> @author GuoPeng </p>
     * <p> @param SignatureDTO ,param 请求入参</p>
     * <p> @time 2021-05-15 14:30</p>
     */
    public Boolean verify(SignatureDTO signatureDTO,
                          String paramJson) {
        //signature
        String signature = encrypt(signatureDTO, paramJson, false);
        if (!signatureDTO.getSignature().equals(signature))
            return false;
        //校验重复提交 //验证是否过期
        if (redisUtil.hasKey(signatureDTO.getNonce() + NONCE)) {
            redisUtil.del(signatureDTO.getNonce() + NONCE);
        } else {
            return false;
        }
        return true;
    }

    /**
     * 定义比较规则
     * @return
     */
    private static Comparator<String> getComparator() {
        return (s1, s2) -> s1.compareTo(s2);
    }

    /**
     * 排序 json排序
     * @param e
     */
    public static void sort(JsonElement e) {
        if (e.isJsonNull() || e.isJsonPrimitive()) {
            return;
        }

        if (e.isJsonArray()) {
            JsonArray a = e.getAsJsonArray();
            Iterator<JsonElement> it = a.iterator();
            it.forEachRemaining(i -> sort(i));
            return;
        }

        if (e.isJsonObject()) {
            Map<String, JsonElement> tm = new TreeMap<>(getComparator());
            for (Map.Entry<String, JsonElement> en : e.getAsJsonObject().entrySet()) {
                tm.put(en.getKey(), en.getValue());
            }

            String key;
            JsonElement val;
            for (Map.Entry<String, JsonElement> en : tm.entrySet()) {
                key = en.getKey();
                val = en.getValue();
                e.getAsJsonObject().remove(key);
                e.getAsJsonObject().add(key, val);
                sort(val);
            }
        }
    }

    /**
     * 根据json key排序
     * @param json
     * @return
     */
    private static String sortJson(String json) {
        if(StringUtils.isEmpty(json)){
            return "null";
        }
        Gson g = new GsonBuilder().setPrettyPrinting().create();
        JsonParser p = new JsonParser();
        JsonElement e = p.parse(json);
        sort(e);
        return g.toJson(e);
    }

}
