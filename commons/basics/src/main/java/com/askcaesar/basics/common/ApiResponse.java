package com.askcaesar.basics.common;

import com.askcaesar.basics.enums.ReturnCode;
import lombok.*;
import lombok.experimental.Accessors;


/**
 * 公共业务返回
 * @author GuoPeng
 */
@Data
@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class ApiResponse<T>{
    /**响应编码：0成功；-1系统异常；*/
    private int code;
    /**响应结果描述*/
    private String message;
    /** 
     * 错误描述
     */
    private String errMsg;
    
    /**业务数据*/
    private T data;

    /**
     * 无业务数据的成功响应
     */
    public static ApiResponse success() {
        return success(null);
    }

    public ApiResponse(int code, String message, String errMsg, T data) {
        this.code = code;
        this.message = message;
        this.errMsg = errMsg;
        this.data = data;
    }

    /**
     * 带业务数据的成功响应
     * @param data
     * @return <T>
     */
    public static <F> ApiResponse<F> success(F data) {
        return new ApiResponse<>(ReturnCode.SUCCESS.getCode(), ReturnCode.SUCCESS.getMsg(), "", data);
    }

    /**
     *判断请求是否成功
     * @return true 成功  false 失败
     */
    public Boolean isSuccess(){
        return ReturnCode.SUCCESS.getCode().equals(this.code);
    }


    /**
     * 响应失败
     * @return
     */
    public static ApiResponse fail(String msg) {
        return fail(ReturnCode.FAIL.getCode(), msg);
    }


    /**
     * 响应失败 带参数 描述
     * @param failCode
     * @param msg
     */
    public static ApiResponse fail(int failCode, String msg) {
        ApiResponse response = new ApiResponse();
        response.setCode(failCode);
        response.setMessage(msg);
        response.setErrMsg(msg);
        return response;
    }
    /**
     * 响应失败
     */
    public static ApiResponse fail() {
        return fail(ReturnCode.FAIL.getCode(), ReturnCode.FAIL.getMsg());
    }

    /**
     * 响应失败 带返回编码
     * @param responseCode
     */
    public static ApiResponse fail(ReturnCode responseCode) {
        return fail(responseCode.getCode(), responseCode.getMsg());
    }

}
