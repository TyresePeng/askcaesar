package com.askcaesar.basics.interceptor;

import com.askcaesar.basics.common.PresentContextInfo;
import com.askcaesar.basics.enums.PresentContextEnum;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ContextHandlerInterceptor  extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)  {
        PresentContextInfo.set(PresentContextEnum.USER_ID.name(), request.getHeader(PresentContextEnum.USER_ID.name()));
        PresentContextInfo.set(PresentContextEnum.USER_NAME.name(), request.getHeader(PresentContextEnum.USER_NAME.name()));
        PresentContextInfo.set(PresentContextEnum.EMAIL.name(), request.getHeader(PresentContextEnum.EMAIL.name()));
        PresentContextInfo.set(PresentContextEnum.APP_ID.name(), request.getHeader(PresentContextEnum.APP_ID.name()));
        return Boolean.TRUE;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
    }

}
