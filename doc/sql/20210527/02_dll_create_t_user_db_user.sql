CREATE TABLE `t_user`
(
    `id`               int(11)     NOT NULL,
    `login_name`       varchar(30) NOT NULL COMMENT '登录名',
    `login_password`   varchar(60) NOT NULL COMMENT '登录密码',
    `user_name`        varchar(80)    DEFAULT NULL COMMENT '用户全名',
    `phone_number`     varchar(20)    DEFAULT NULL COMMENT '手机号',
    `email`            varchar(50)    DEFAULT NULL COMMENT '邮箱地址',
    `remark`           varchar(600)   DEFAULT NULL COMMENT '备注说明',
    `created_by`       varchar(36)    DEFAULT NULL COMMENT '创建人',
    `created_date`     datetime       DEFAULT NULL COMMENT '创建时间',
    `modified_by`      varchar(36)    DEFAULT NULL COMMENT '修改人',
    `modified_date`    decimal(13, 0) DEFAULT NULL COMMENT '修改时间',
    `user_avatar_path` varchar(200)   DEFAULT NULL COMMENT '用户头像上传记录Id',
    `last_login_time`  datetime       DEFAULT NULL COMMENT '上次登录时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='用户表';