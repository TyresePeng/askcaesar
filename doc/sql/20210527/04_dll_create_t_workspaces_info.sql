-- 工作空间信息
create table workspaces_info
(
    id              int(11) auto_increment comment '主键' primary key,
    workspaces_name varchar(255)     not null comment '工作空间名称',
    summary         varchar(1000)    null comment '空间描述',
    workspaces_type varchar(20)      not null comment '工作空间类型（团队1/个人2）',
    public_type     bit default b'0' not null comment '工作空间开放类型 （0私有 1开源 ）',
    create_user_id  int(11)          not null comment '创建人id',
    create_time     datetime         not null comment '创建时间',
    delete_bit      bit default b'0' not null comment '逻辑删除'
)
    comment '工作空间信息';

create index INDEX_USER_ID
    on workspaces_info (create_user_id)
    comment '创建人索引';

