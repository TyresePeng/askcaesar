-- 工作空间成员
create table workspaces_member_info
(
    id            int(11) auto_increment comment '主键' primary key,
    user_id       int(11)          not null comment '用户id',
    workspaces_Id int(11)          not null comment '工作空间id',
    user_role     int(4)           not null comment '所属工作空间权限 (admin dev look)',
    add_user_id   int(11)          not null comment '添加人id',
    add_time      datetime         not null comment '添加时间',
    delete_bit    bit default b'0' not null comment '逻辑删除',
    constraint workspaces_member_info_ibfk_1
        foreign key (workspaces_Id) references workspaces_info (id)
)
    comment '工作空间成员';

create index workspaces_Id
    on workspaces_member_info (workspaces_Id);

